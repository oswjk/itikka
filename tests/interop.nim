import unittest
import asyncdispatch

import itikka

const
    topics = @[
        "TopicA",
        "TopicA/B",
        "Topic/C",
        "TopicA/C",
        "/TopicA"
    ]

    wildTopics = @[
        "TopicA/+",
        "+/C",
        "#",
        "/#",
        "/+",
        "+/+",
        "TopicA/#"
    ]

    nosubscribeTopics = @[
        "test/nosubscribe"
    ]

    host = "localhost"
    # host = "test.mosquitto.org"
    port = 1883.Port

proc runWhilePending() =
    while hasPendingOperations():
        poll()

suite "interoperability tests":
    # TODO: setup/teardown cleanup server

    setup:
        proc cleanupServer() {. async .} =
            # Clean client state

            var client = newClient("clienta")
            await client.connect(host, port, 60, true)
            await client.disconnect()

            client = newClient("clientb")
            await client.connect(host, port, 60, true)
            await client.disconnect()

            # Clean retained messages

            client = newClient("clean-retained")
            await client.connect(host, port, 60, true)
            proc cb(message: Message) {. async .} =
                discard await client.publish(message.topic, "", Qos0, true)
            client.onMessageReceived = cb
            discard await client.subscribe(@[
                Subscription(topic: "#", qos: Qos0)
            ])
            discard await client.process().withTimeout(2000)
            await client.disconnect()

        waitFor cleanupServer()
        runWhilePending()

    test "connecting works":
        var client = newClient("clienta")
        waitFor client.connect(host, port, 60, true)
        waitFor client.disconnect()

    test "basics":
        proc main() {. async .} =
            var client = newClient("clienta")
            var messageCount = 0
            proc cb(message: Message) {. async .} =
                inc(messageCount)
            client.onMessageReceived = cb
            await client.connect(host, port, 60, true)
            discard await client.subscribe(@[
                Subscription(topic: topics[0], qos: Qos2)
            ])
            discard await client.publish(topics[0], "msg0", Qos0)
            discard await client.publish(topics[0], "msg1", Qos1)
            discard await client.publish(topics[0], "msg2", Qos2)
            discard await client.process().withTimeout(2000)
            await client.disconnect()
            check(messageCount == 3)

        waitFor main()
        runWhilePending()

    test "retained messages":
        proc main() {. async .} =
            var client = newClient("retained_message_test")

            var messageCount = 0
            proc cb(message: Message) {. async .} =
                inc(messageCount)
            client.onMessageReceived = cb

            await client.connect(host, port, 60, true)

            discard await client.publish(topics[1], "qos0", Qos0, true)
            discard await client.publish(topics[2], "qos1", Qos1, true)
            discard await client.publish(topics[3], "qos2", Qos2, true)

            discard await client.subscribe(@[
                Subscription(topic: wildTopics[5], qos: Qos2)
            ])

            discard await client.process().withTimeout(2000)
            await client.disconnect()

            check(messageCount == 3)

        waitFor main()
        runWhilePending()

    test "will":
        proc main() {. async .} =
            var clientA = newClient("clienta")
            clientA.setWill(topics[2], "client disconnected", Qos0, false)
            await clientA.connect(host, port, 60, true)

            var clientB = newClient("clientb")
            await clientB.connect(host, port, 60, true)
            discard await clientB.subscribe(@[
                Subscription(topic: topics[2], qos: Qos2)
            ])

            var messageCount = 0
            proc cb(message: Message) {. async .} =
                inc(messageCount)
            clientB.onMessageReceived = cb

            await sleepAsync(500)
            clientA.closeSocket()

            discard await clientB.process().withTimeout(1000)
            await clientB.disconnect()

            check(messageCount == 1)

        waitFor main()
        runWhilePending()

    test "zero length clientid not allowed with cleanSession=false":
        var client = newClient("")
        expect ConnectionError:
            waitFor client.connect(host, port, 60, false)

    test "zero length clientid allowed with cleanSession=true":
        var client = newClient("")
        waitFor client.connect(host, port, 60, true)
        waitFor client.disconnect()

    test "offline message queueing":
        proc main() {. async .} =
            var clienta = newClient("clienta")
            await clienta.connect(host, port, 60, false)
            discard await clienta.subscribe(@[
                Subscription(topic: wildTopics[5], qos: Qos2)
            ])
            await clienta.disconnect()

            var clientb = newClient("clientb")
            await clientb.connect(host, port, 60, true)
            discard await clientb.publish(topics[1], "qos0", Qos0, false)
            discard await clientb.publish(topics[2], "qos1", Qos1, false)
            discard await clientb.publish(topics[3], "qos2", Qos2, false)
            await clientb.disconnect()

            clienta = newClient("clienta")
            var messageCount = 0
            proc cb(message: Message) {. async .} =
                inc(messageCount)
            clienta.onMessageReceived = cb
            await clienta.connect(host, port, 60, false)
            discard await clienta.process().withTimeout(2000)
            await clienta.disconnect()

            check((messageCount == 2) or (messageCount == 3))

        waitFor main()
        runWhilePending()

    test "overlapping subscriptions":
        proc main() {. async .} =
            var client = newClient("clienta")

            var messages: seq[Message] = @[]
            proc cb(message: Message) {. async .} =
                messages.add(message)
            client.onMessageReceived = cb

            await client.connect(host, port, 60, true)

            let subid = await client.subscribe(@[
                Subscription(topic: wildTopics[6], qos: Qos2),
                Subscription(topic: wildTopics[0], qos: Qos1),
            ])

            check(subid == @[Qos2, Qos1])

            discard await client.publish(topics[3], "overlapping topic filters", Qos2)

            discard await client.process().withTimeout(1000)
            await client.disconnect()

            if len(messages) == 1:
                check(messages[0].qos == Qos2)
            else:
                check(((messages[0].qos == Qos2) and (messages[1].qos == Qos1)) or
                      ((messages[0].qos == Qos1) and (messages[1].qos == Qos2)))

        waitFor main()
        runWhilePending()

    test "keepalive":
        proc main() {. async .} =
            var clienta = newClient("clienta")
            clienta.setWill(topics[4], "client disconnected", Qos0, false)
            await clienta.connect(host, port, 2, true)

            var clientb = newClient("clientb")

            var messages: seq[Message] = @[]
            proc cb(message: Message) {. async .} =
                messages.add(message)
            clientb.onMessageReceived = cb

            await clientb.connect(host, port, 0, true)
            discard await clientb.subscribe(@[Subscription(topic: topics[4],
                qos: Qos2)])

            discard await clientb.process().withTimeout(5000)

            await clienta.disconnect()
            await clientb.disconnect()

            check(len(messages) == 1)

        waitFor main()
        runWhilePending()

    test "redelivery on reconnect":
        proc main1() {. async .} =
            var client = newClient("clientb")

            await client.connect(host, port, 60, false)

            var messages: seq[Message] = @[]
            proc cb(message: Message) {. async .} =
                messages.add(message)
            client.onMessageReceived = cb

            discard await client.subscribe(@[Subscription(topic: wildTopics[6],
                qos: Qos2)])

            client.dontHandlePublish = true

            discard await client.publish(topics[1], "", Qos1, false)
            discard await client.publish(topics[3], "", Qos2, false)

            discard await client.process().withTimeout(1000)

            await client.disconnect()

            check(len(messages) == 0)

        proc main2() {. async .} =
            var client = newClient("clientb")

            var messages: seq[Message] = @[]
            proc cb(message: Message) {. async .} =
                messages.add(message)
            client.onMessageReceived = cb

            await client.connect(host, port, 60, false)

            discard await client.process().withTimeout(1000)

            check(len(messages) == 2)

            await client.disconnect()

        waitFor main1()
        waitFor main2()
        runWhilePending()

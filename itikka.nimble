# Package

version       = "0.1.0"
author        = "Oskari Timperi"
description   = "MQTT client library for Nim"
license       = "MIT"

# Dependencies

requires "nim >= 0.17.0"

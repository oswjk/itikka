import itikka
import asyncdispatch
import parseopt2
import strutils

proc main() {. async .} =
    var
        clientId = "itikkapub2"
        host = "test.mosquitto.org"
        port = 1883.Port
        keepAlive: uint16 = 60
        cleanSession = true
        topic: string
        message: string
        qos = Qos0
        retain = false

    for kind, key, val in getopt():
        case kind
        of cmdLongOption:
            case key
            of "id":
                clientId = val
            of "host":
                host = val
            of "port":
                port = Port(parseInt(val))
            of "keepalive":
                keepAlive = uint16(parseInt(val))
            of "topic":
                topic = val
            of "message":
                message = val
            of "qos":
                qos = Qos(parseInt(val))
            of "retain":
                retain = true
            of "clean":
                cleanSession = true
        else:
            discard

    if (topic == nil) or (message == nil):
        echo("topic and message must be provided")
        quit(1)

    var client = newClient(clientId)

    await client.connect(host, port, keepAlive, cleanSession)

    discard await client.publish(topic, message, qos, retain)

    await client.disconnect()

waitFor(main())

import itikka
import asyncdispatch
import parseopt2
import strutils

proc main() {. async .} =
    var
        clientId = "itikka-subscriber"
        host = "test.mosquitto.org"
        port = 1883.Port
        keepAlive: uint16 = 60
        cleanSession = true
        subscriptions: seq[Subscription] = @[]

    for kind, key, val in getopt():
        case kind
        of cmdLongOption:
            case key
            of "id":
                clientId = val
            of "host":
                host = val
            of "port":
                port = Port(parseInt(val))
            of "keepalive":
                keepAlive = uint16(parseInt(val))
            of "clean":
                cleanSession = true
        of cmdArgument:
            subscriptions.add(Subscription(topic: key, qos: Qos0))
        else:
            discard

    var client = newClient(clientId)

    await client.connect(host, port, keepAlive, cleanSession)

    proc cb(message: Message) {. async .} =
        echo(format("$1 - $2", message.topic, message.payload))

    client.onMessageReceived = cb

    discard await client.subscribe(subscriptions)

    await client.process()

    await client.disconnect()

waitFor(main())

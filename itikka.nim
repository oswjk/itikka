import asyncdispatch
import asyncnet
import endians
import nativesockets
import streams
import strutils
import tables
import times

type
    ClientState {. pure .} = enum
        Disconnected
        Connected

    Qos* = enum
        Qos0 = 0
        Qos1 = 1
        Qos2 = 2
        SubscriptionFailure = 0x80

    PacketKind {. pure .} = enum
        Invalid = 0x0
        Connect = 0x1
        ConnAck = 0x2
        Publish = 0x3
        PubAck = 0x4
        PubRec = 0x5
        PubRel = 0x6
        PubComp = 0x7
        Subscribe = 0x8
        SubAck = 0x9
        Unsubscribe = 0xA
        UnsubAck = 0xB
        PingReq = 0xC
        PingResp = 0xD
        Disconnect = 0xE

    Packet = ref object of RootObj
        kind: PacketKind
        flags: uint8
        id: uint16
        payload: StringStream

    MessageState {. pure .} = enum
        Queued
        Publish
        WaitPubAck
        WaitPubRec
        WaitPubComp
        WaitPubRel

    Message* = ref object of RootObj
        qos*: Qos
        retain*: bool
        dup*: bool
        topic*: string
        payload*: string
        id*: uint16
        state: MessageState
        timestamp: float
        future: Future[void]

    Client* = ref object of RootObj
        socket: AsyncSocket
        clientId: string
        cleanSession: bool
        host: string
        port: Port
        keepAlive: uint16
        sessionPresent: bool
        state: ClientState
        willQos: Qos
        willRetain: bool
        willTopic: string
        willMessage: string
        userName: string
        password: string
        lastPacketSentTime: float
        outMessages: OrderedTable[uint16, Message]
        inMessages: OrderedTable[uint16, Message]
        onMessageReceived*: OnMessageReceivedCb
        pingSent: bool
        packetId: uint16
        maxQueuedMessages: int
        maxInflightMessages: int
        retryTimeout: float
        subscriptionFutures: Table[uint16, Future[seq[Qos]]]
        unsubscriptionFutures: Table[uint16, Future[void]]
        recvFuture: Future[Packet]
        sendLock: AsyncMutex
        recvLock: AsyncMutex
        processingLock: AsyncMutex
        when defined(testing):
            dontHandlePublish*: bool

    WrongPacketError = object of Exception

    ConnectionErrorCode* {. pure .} = enum
        Ok = 0
        UnacceptableProtocolVersion = (1, "unacceptable protocol version")
        IdentifierRejected = (2, "identifier rejected")
        ServerUnavailable = (3, "server unavailable")
        BadUsernameOrPassword = (4, "bad username or password")
        NotAuthorized = (5, "not authorized")

    ConnectionError* = object of Exception
        rc*: ConnectionErrorCode

    DisconnectedError* = object of Exception

    OnMessageReceivedCb* =
        proc (msg: Message): Future[void] {. closure, gcsafe .}

    Subscription* = object
        topic*: string
        qos*: Qos

    AsyncMutex = ref object
        locked: bool
        futures: seq[Future[void]]

const
    MqttProtocolId = "MQTT"
    MqttProtocolLevel = 0x04.uint8

proc newAsyncMutex(): AsyncMutex =
    new(result)
    result.locked = false
    result.futures = @[]

proc lock(mutex: AsyncMutex) {. async .} =
    if not mutex.locked:
        mutex.locked = true
    else:
        var future = newFuture[void]()
        mutex.futures.add(future)
        await future

proc unlock(mutex: AsyncMutex) =
    if len(mutex.futures) > 0:
        var future = mutex.futures[0]
        mutex.futures.delete(0)
        future.complete()
    else:
        mutex.locked = false

proc newPacket(kind: PacketKind): Packet =
    new(result)
    result.kind = kind
    result.payload = newStringStream()

proc newMessage*(topic, payload: string, qos: Qos = Qos0,
                 retain: bool = false): Message =
    new(result)
    result.topic = topic
    result.payload = payload
    result.qos = qos
    result.retain = retain

proc newWrongPacketError(got, expected: PacketKind): ref WrongPacketError =
    result = newException(WrongPacketError,
        format("got $1, expected $2", got, expected))

proc newConnectionError(rc: ConnectionErrorCode): ref ConnectionError =
    result = newException(ConnectionError, $rc)
    result.rc = rc

proc writeBigEndian(stream: Stream, value: uint16) =
    var
        src = value
        dst = value
    bigEndian16(addr dst, addr src)
    stream.write(dst)

proc writeMqttString(stream: Stream, str: string) =
    stream.writeBigEndian(len(str).uint16)
    stream.write(str)

proc readBigEndian(stream: Stream): uint16 =
    var
        src: uint16
        dst: uint16
    src = stream.readInt16().uint16
    bigEndian16(addr dst, addr src)
    result = dst

proc readMqttString(stream: Stream): string =
    let len = stream.readBigEndian().int
    result = stream.readStr(len)

proc newClient*(clientId: string): Client =
    new(result)
    result.clientId = clientId
    result.state = ClientState.Disconnected
    result.maxQueuedMessages = 0
    result.maxInflightMessages = 0
    result.outMessages = initOrderedTable[uint16, Message]()
    result.inMessages = initOrderedTable[uint16, Message]()
    result.retryTimeout = 20
    result.subscriptionFutures = initTable[uint16, Future[seq[Qos]]]()
    result.unsubscriptionFutures = initTable[uint16, Future[void]]()
    result.recvLock = newAsyncMutex()
    result.sendLock = newAsyncMutex()
    result.processingLock = newAsyncMutex()

    when defined(testing):
        result.dontHandlePublish = false

proc connectFlags(client: Client): uint8 =
    result = 0

    if client.cleanSession:
        result = result or 0x02

    if client.willTopic != nil:
        result = result or 0x04
        result = result or ((client.willQos.uint8 and 3) shl 3)
        result = result or ((client.willRetain.uint8 and 1) shl 5)

    if client.userName != nil:
        result = result or 0x80
        if client.password != nil:
            result = result or 0x40

proc sendByte(socket: AsyncSocket, b: uint8) {. async .} =
    var tmp = b
    await socket.send(addr tmp, sizeof tmp, {})

proc sendRemainingLength(socket: AsyncSocket, len: int) {. async .} =
    var tmp = len
    while true:
        var encodedByte = (tmp mod 128).uint8
        tmp = tmp div 128
        if tmp > 0:
            encodedByte = encodedByte or 128'u8
        await socket.sendByte(encodedByte)
        if tmp == 0:
            break

template awaitLocked(lock: AsyncMutex, exp: typed): untyped =
    # This is braindead. :-) Await should just work in try..finally.
    var future = exp
    yield future
    if future.failed:
        lock.unlock()
        raise future.readError()
    future.read()

proc sendPacket(client: Client, packet: Packet) {. async .} =
    await client.sendLock.lock()

    var typeAndFlags: uint8 = ((packet.kind.uint8 and 0x0F) shl 4) or
                               (packet.flags and 0x0F)

    awaitLocked(client.sendLock, client.socket.sendByte(typeAndFlags))

    awaitLocked(client.sendLock,
        client.socket.sendRemainingLength(len(packet.payload.data)))

    if len(packet.payload.data) > 0:
        awaitLocked(client.sendLock,
            client.socket.send(packet.payload.data, {}))

    client.lastPacketSentTime = epochTime()

    client.sendLock.unlock()

proc recvByte(socket: AsyncSocket): Future[byte] {. async .} =
    let s = await socket.recv(1)
    if len(s) == 0:
        raise newException(DisconnectedError, "disconnected")
    else:
        result = s[0].byte

proc recvRemainingLength(socket: AsyncSocket): Future[int] {. async .} =
    var
        encodedByte: byte
        mul: int = 1

    while true:
        encodedByte = await socket.recvByte()
        result = result + (encodedByte and 127).int * mul
        if mul > 128*128*128:
            raise newException(Exception, "too big remaining length")
        mul = mul * 128
        if (encodedByte and 128) == 0:
            break

proc recvPacket(client: Client): Future[Packet] {. async .} =
    await client.recvLock.lock()

    let typeAndFlags = awaitLocked(client.recvLock, client.socket.recvByte())

    new(result)

    result.kind = ((typeAndFlags shr 4) and 0x0F).PacketKind
    result.flags = typeAndFlags and 0x0F

    if (result.kind.uint8 < PacketKind.Connect.uint8) or
       (result.kind.uint8 > PacketKind.Disconnect.uint8):
       raise newException(Exception, "invalid packet type received")

    let remainingLength = awaitLocked(client.recvLock,
        client.socket.recvRemainingLength())

    if remainingLength > 0:
        let payload = awaitLocked(client.recvLock,
            client.socket.recv(remainingLength))

        if len(payload) != remainingLength:
            raise newException(DisconnectedError, "disconnected")

        result.payload = newStringStream(payload)

    client.recvLock.unlock()

proc connect*(client: Client, host: string, port: Port, keepAlive: uint16,
              cleanSession: bool) {. async .} =
    if client.state != ClientState.Disconnected:
        raise newException(Exception, "client must be disconnected to connect")

    client.host = host
    client.port = port
    client.keepAlive = keepAlive
    client.cleanSession = cleanSession

    client.pingSent = false
    client.inMessages.clear()
    client.outMessages.clear()
    client.packetId = 0

    client.socket = await asyncnet.dial(host, port)

    let packet = newPacket(PacketKind.Connect)

    packet.payload.writeMqttString(MqttProtocolId)
    packet.payload.write(MqttProtocolLevel)
    packet.payload.write(client.connectFlags)
    packet.payload.writeBigEndian(client.keepAlive)
    packet.payload.writeMqttString(client.clientId)

    if client.willTopic != nil:
        packet.payload.writeMqttString(client.willTopic)
        packet.payload.writeMqttString(client.willMessage)

    if client.userName != nil:
        packet.payload.writeMqttString(client.userName)
        if client.password != nil:
            packet.payload.writeMqttString(client.password)

    await client.sendPacket(packet)

    let reply = await client.recvPacket()

    if reply.kind != PacketKind.ConnAck:
        client.socket.close()
        raise newWrongPacketError(reply.kind, PacketKind.ConnAck)

    let flags = reply.payload.readInt8().uint8
    let rc = reply.payload.readInt8().uint8

    if rc != 0:
        client.socket.close()
        raise newConnectionError(rc.ConnectionErrorCode)

    client.sessionPresent = (flags and 0x1).bool

    client.state = ClientState.Connected

proc isConnected(client: Client): bool =
    result = (client.state == ClientState.Connected)

when defined(testing):
    proc closeSocket*(client: Client) =
        client.socket.close()

proc disconnect*(client: Client) {. async .} =
    if not client.isConnected():
        raise newException(Exception, "must be connected to disconnect")

    let packet = newPacket(PacketKind.Disconnect)

    try:
        await client.sendPacket(packet)
    except:
        # Server has probably closed the connection
        discard

    client.socket.close()

    client.state = ClientState.Disconnected

proc sendPingReq(client: Client) {. async .} =
    if not client.isConnected():
        raise newException(Exception, "must be connected to ping")
    let req = newPacket(PacketKind.PingReq)
    await client.sendPacket(req)
    client.pingSent = true

proc sendPubRel(client: Client, message: Message) {. async .} =
    let packet = newPacket(PacketKind.PubRel)
    packet.flags = 0x02
    packet.payload.writeBigEndian(message.id)
    await client.sendPacket(packet)
    message.timestamp = epochTime()

proc sendPubComp(client: Client, message: Message) {. async .} =
    let packet = newPacket(PacketKind.PubComp)
    packet.payload.writeBigEndian(message.id)
    await client.sendPacket(packet)
    message.timestamp = epochTime()

proc sendPubAck(client: Client, message: Message) {. async .} =
    let packet = newPacket(PacketKind.PubAck)
    packet.payload.writeBigEndian(message.id)
    await client.sendPacket(packet)
    message.timestamp = epochTime()

proc sendPubRec(client: Client, message: Message) {. async .} =
    let packet = newPacket(PacketKind.PubRec)
    packet.payload.writeBigEndian(message.id)
    await client.sendPacket(packet)
    message.timestamp = epochTime()

proc assertMessageState(message: Message, allowed: set[MessageState]) =
    if message.state notin allowed:
        raise newException(Exception,
            format("message expected to be in state $1, was in $2",
                allowed, message.state))

proc handleSubAck(client: Client, packet: Packet) =
    let packetId = packet.payload.readBigEndian()

    var qos: seq[Qos] = @[]

    while not packet.payload.atEnd():
        let value = packet.payload.readInt8().Qos
        qos.add(value)

    client.subscriptionFutures[packetId].complete(qos)

proc handleUnsubAck(client: Client, packet: Packet) =
    let packetId = packet.payload.readBigEndian()
    client.unsubscriptionFutures[packetId].complete()

proc handlePubAck(client: Client, packet: Packet) {. async .} =
    let packetId = packet.payload.readBigEndian()

    if not client.outMessages.hasKey(packetId):
        raise newException(Exception,
            format("received puback for unknown message id $1", packetId))

    var message = client.outMessages[packetId]
    client.outMessages.del(packetId)

    assertMessageState(message, {MessageState.WaitPubAck})

    message.future.complete()

proc handlePubRec(client: Client, packet: Packet) {. async .} =
    let packetId = packet.payload.readBigEndian()

    if not client.outMessages.hasKey(packetId):
        raise newException(Exception,
            format("received pubrec for unknown message id $1", packetId))

    var message = client.outMessages[packetId]

    assertMessageState(message, {MessageState.WaitPubRec,
        MessageState.WaitPubComp})

    await client.sendPubRel(message)

    message.state = MessageState.WaitPubComp

proc handlePubComp(client: Client, packet: Packet) {. async .} =
    let packetId = packet.payload.readBigEndian()

    if not client.outMessages.hasKey(packetId):
        raise newException(Exception,
            format("received puback for unknown message id $1", packetId))

    var message = client.outMessages[packetId]
    client.outMessages.del(packetId)

    assertMessageState(message, {MessageState.WaitPubComp})

    message.future.complete()

proc handlePubRel(client: Client, packet: Packet) {. async .} =
    let packetId = packet.payload.readBigEndian()

    if not client.inMessages.hasKey(packetId):
        raise newException(Exception,
            format("received pubrel for unknown message id $1", packetId))

    var message = client.inMessages[packetId]
    client.inMessages.del(packetId)

    assertMessageState(message, {MessageState.WaitPubRel})

    await client.sendPubComp(message)

proc handlePublish(client: Client, packet: Packet) {. async .} =
    when defined(testing):
        if client.dontHandlePublish:
            return

    let dup = ((packet.flags shr 3) and 1).bool
    let qos = ((packet.flags shr 1) and 3).Qos
    let retain = (packet.flags and 1).bool

    let topic = packet.payload.readMqttString()

    var packetId: uint16
    if qos != Qos0:
        packetId = packet.payload.readBigEndian()

    let payload = packet.payload.readAll()

    if qos == Qos2:
        # Check if we have sent pubrec for this message. Resend if necessary.
        if client.inMessages.hasKey(packetId):
            var message = client.inMessages[packetId]
            if message.state == MessageState.WaitPubRel:
                await client.sendPubRec(message)
                return

    var message: Message
    new(message)

    message.qos = qos
    message.retain = retain
    message.dup = dup
    message.topic = topic
    message.payload = payload
    message.id = packetId

    if client.onMessageReceived != nil:
        await client.onMessageReceived(message)

    if qos == Qos1:
        await client.sendPubAck(message)
    elif qos == Qos2:
        message.state = MessageState.WaitPubRel
        client.inMessages.add(packetId, message)
        await client.sendPubRec(message)

proc handlePacket(client: Client, packet: Packet) {. async .} =
    case packet.kind
    of PacketKind.PingResp:
        client.pingSent = false
    of PacketKind.SubAck:
        client.handleSubAck(packet)
    of PacketKind.UnsubAck:
        client.handleUnsubAck(packet)
    of PacketKind.PubAck:
        await client.handlePubAck(packet)
    of PacketKind.PubRec:
        await client.handlePubRec(packet)
    of PacketKind.PubComp:
        await client.handlePubComp(packet)
    of PacketKind.PubRel:
        await client.handlePubRel(packet)
    of PacketKind.Publish:
        await client.handlePublish(packet)
    else:
        discard

proc nextPacketId(client: Client): uint16 =
    inc(client.packetId)
    result = client.packetId

proc shouldResend(client: Client, message: Message): bool =
    if message.timestamp > 0.0:
        let elapsed = epochTime() - message.timestamp
        if elapsed >= client.retryTimeout:
            result = true

proc sendPublish(client: Client, message: Message) {. async .} =
    let packet = newPacket(PacketKind.Publish)

    packet.payload.writeMqttString(message.topic)

    if message.qos != Qos0:
        packet.payload.writeBigEndian(message.id.uint16)

    packet.payload.write(message.payload)

    packet.flags = (message.qos.uint8 and 3) shl 1
    packet.flags = packet.flags or ((message.dup.uint8 and 1) shl 3)
    packet.flags = packet.flags or (message.retain.uint8 and 1)

    await client.sendPacket(packet)

    message.timestamp = epochTime()

proc processOutMessages(client: Client) {. async .} =
    var inflight = len(client.inMessages) + len(client.outMessages)

    for msg in client.outMessages.values():
        if msg.state == MessageState.Queued:
            dec(inflight)

    for msg in client.outMessages.values():
        case msg.state
        of MessageState.Queued:
            if (client.maxInflightMessages > 0) and
               (inflight >= client.maxInflightMessages):
               continue
            await client.sendPublish(msg)
            if msg.qos == Qos1:
                msg.state = MessageState.WaitPubAck
            else:
                msg.state = MessageState.WaitPubRec
            inc(inflight)
        of MessageState.WaitPubAck, MessageState.WaitPubRec:
            if client.shouldResend(msg):
                msg.dup = true
                await client.sendPublish(msg)
        of MessageState.WaitPubComp:
            if client.shouldResend(msg):
                await client.sendPubRel(msg)
        else:
            discard

proc processInMessages(client: Client) {. async .} =
    for msg in client.inMessages.values():
        if msg.state == MessageState.WaitPubRel:
            if client.shouldResend(msg):
                await client.sendPubRec(msg)

proc processOnceImpl[T](client: Client, derpFuture: Future[T]) {. async .} =
    if not client.isConnected():
        if not client.socket.isClosed():
            client.socket.close()
        return

    # Cannot use defer. Might not be able to use try/finally either
    # (https://github.com/nim-lang/Nim/issues/4116). So we need to unlock the
    # lock manully when returning from this proc.
    await client.processingLock.lock()

    # This is to avoid the situation that proc A has called processOnce and they
    # are blocking on the above lock, meanwhile processOnce invoked by proc B
    # finishes A's future. Now if A gets the lock above after B is done and
    # continue doing a recvPacket, we could be waiting on that recvPacket for
    # keepAlive seconds while A's future has already been completed.
    if (derpFuture != nil) and derpFuture.finished:
        client.processingLock.unlock()
        return

    awaitLocked(client.processingLock, client.processOutMessages())
    awaitLocked(client.processingLock, client.processInMessages())

    if client.recvFuture == nil:
        client.recvFuture = client.recvPacket()

    let hasPacket = awaitLocked(client.processingLock,
        client.recvFuture.withTimeout(250))

    if hasPacket:
        var packet: Packet

        try:
            packet = client.recvFuture.read()
        except DisconnectedError:
            client.socket.close()
            if client.state != ClientState.Disconnected:
                client.state = ClientState.Disconnected
                client.processingLock.unlock()
                raise
            client.processingLock.unlock()
            return
        finally:
            client.recvFuture = nil

        awaitLocked(client.processingLock, client.handlePacket(packet))

        if not client.isConnected():
            client.socket.close()
    else:
        if client.isConnected() and (client.keepAlive.int > 0):
            let now = epochTime()
            if now - client.lastPacketSentTime >= client.keepAlive.float:
                if client.pingSent:
                    client.processingLock.unlock()
                    raise newException(Exception, "no pingresp received in time")
                awaitLocked(client.processingLock, client.sendPingReq())

    client.processingLock.unlock()

proc processOnce*(client: Client): Future[void] =
    result = processOnceImpl[void](client, nil)

proc process*(client: Client) {. async .} =
    while client.state != ClientState.Disconnected:
        await client.processOnce()

proc subscribe*(client: Client, subs: seq[Subscription]): Future[seq[Qos]] {. async .} =
    let req = newPacket(PacketKind.Subscribe)
    req.flags = 0x2

    let packetId = client.nextPacketId()

    req.payload.writeBigEndian(packetId)

    for sub in subs:
        req.payload.writeMqttString(sub.topic)
        req.payload.write(sub.qos.uint8)

    await client.sendPacket(req)

    var subFut = newFuture[seq[Qos]]("itikka.subscribe")

    client.subscriptionFutures.add(packetId, subFut)

    while (not subFut.finished) and client.isConnected():
        await client.processOnceImpl(subFut)

    client.subscriptionFutures.del(packetId)

    if subFut.finished:
        result = subFut.read()
    elif not client.isConnected():
        result = @[]

proc unsubscribe*(client: Client, topicFilters: seq[string]) {. async .} =
    let req = newPacket(PacketKind.Unsubscribe)
    req.flags = 0x2

    let packetId = client.nextPacketId()

    req.payload.writeBigEndian(packetId)

    for filter in topicFilters:
        req.payload.writeMqttString(filter)

    await client.sendPacket(req)

    var future = newFuture[void]("itikka.unsubscribe")
    client.unsubscriptionFutures.add(packetId, future)

    while (not future.finished) and client.isConnected():
        await client.processOnceImpl(future)

    client.unsubscriptionFutures.del(packetId)

    if future.finished:
        future.read()

proc setWill*(client: Client, topic: string, message: string, qos: Qos,
             retain: bool) =
    ## Set Last Will and Testament (LWT).
    ##
    ## This proc must be called before calling `connect`.
    if client.isConnected():
        raise newException(Exception, "will must be set before connecting")

    client.willTopic = topic
    client.willMessage = message
    client.willQos = qos
    client.willRetain = retain

proc clearWill*(client: Client) =
    ## Clear the LWT.
    client.willTopic = nil
    client.willMessage = nil

proc setAuth*(client: Client, userName: string, password: string) =
    ## Set username and password.
    ##
    ## This proc must be called before calling `connect`. To set only the
    ## username, provide `nil` for the password.
    if client.isConnected():
        raise newException(Exception,
            "auth settings must be set before connecting")

    if userName != nil:
        client.userName = userName
        client.password = password
    else:
        client.userName = nil
        client.password = nil

proc clearAuth*(client: Client) =
    ## Clear username and password.
    client.userName = nil
    client.password = nil

proc publish*(client: Client, message: Message) {. async .} =
    ## Publish a message.
    ##
    ## The returned Future completes when the message has
    ## been queued internally or sent to the network in the case of Qos0
    ## message.
    ##
    ## The message instance will have its `id` updated (0 for Qos0 and an
    ## incrementing unsigned int for Qos1 and Qos2).
    message.id = 0
    message.dup = false
    if message.qos != Qos0:
        if (client.maxQueuedMessages > 0) and
           (len(client.outMessages) >= client.maxQueuedMessages):
           raise newException(Exception, "message queue full")

        message.id = client.nextPacketId()
        message.future = newFuture[void]("itikka.publish")
        message.state = MessageState.Queued
        client.outMessages.add(message.id, message)

        while (not message.future.finished) and client.isConnected():
            await client.processOnceImpl(message.future)

        if message.future.finished:
            message.future.read()
    else:
        await client.sendPublish(message)

proc publish*(client: Client, topic: string, message: string, qos: Qos = Qos0,
              retain: bool = false): Future[uint16] {. async .} =
    ## Convenience proc for sending messages without constructing a Message
    ## instance yourself. The returned Future will be completed the same way as
    ## for the other `publish` proc. The Future will contain the message id of
    ## the sent message.
    let msg = newMessage(topic, message, qos, retain)
    await client.publish(msg)
    result = msg.id
